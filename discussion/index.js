//node.js introduction

//check node version on terminal using : node --version

/*
	use the "require" derective to load node.js modules

	a "module" is a software componeent or part of aprogram that contains one or more routines 

	the "http module" lets node,js transfer data using the hyper texct transfer protocol

	the "http module" is a set of individual files that contain code to create a component that helps establish dta transfer between applications.

	HTTP is a protocol that allows the fetchiong tof resources such as HTML documents

	clietns (browser) and server communicate by exchanging individual messages 

	the messages sent byu the cilemt, usually a web browser are called requests

	the messages sent by the servers as an answer are called responses

*/
let http = require("http");

//using this module createServer() method, we can create an HTTp server that listen to request on a specified port and gives responses bact ot the client
//the http module has a createServer() method that accpets a function as an argument and allows for a creation of a server
//arguments passed in the function are request and respones object (data type ) that  contains methods that allows us to receive request from the client and send responses back to it
http.createServer( function (request, response) {

	//use the writeHead
		//set status code for the response. 200 means OK
		//set the content-type of the response as a plain text meassage
	response.writeHead(200, {'Content-Type': 'text/plain'});
	//send the response witrh a text content 'hello world'
	response.end('hello world');


// a port is a virtual point wehere  network connection start and end
//each port is associated with a specific process aor servbice
//the server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any request that are setn to it, eventually communicating with our server	
} ).listen(4000);



console.log('server running at localhost:4000');


















































































































