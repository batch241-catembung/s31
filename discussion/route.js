const http = require('http');

const port = 4000;

const server  = http.createServer(function(request, response){

	if(request.url == '/greeting'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('hello world')
	}
	else if(request.url == '/homepage'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('welcome to the homepage!')
	}
	else{
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('page not available')
	}

});

server.listen(port);

console.log(`server is now accessible at localhost: ${port}`);